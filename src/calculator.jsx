import { useState } from "react";
import { evaluate } from "mathjs";

export const numbers = [0,1,2,3,4,5,6,7,8,9]
export const equalSign = '='
export const operations = ['+', '-', '*', '/']
const rows = [
    [7, 8, 9],
    [4, 5, 6],
    [1, 2, 3],
    [0]
]



export function Calculator(){
    const [value, setValue] = useState('')

    function createHandleClick(str){
        setValue(value.concat(str))
    }

    return (
        <section>
            <h1>Calculator</h1>
            <input type="text" value={value} readOnly />
            <div role="grid">
                {rows.map(function(row, idx){
                    return (
                        <div key={idx} role="row">
                            {row.map(function(number){
                                return (<button key={number} onClick={() => createHandleClick(number)}>{number}</button>)
                            })}
                        </div>
                    )
                })}
                {operations.map(function(op){
                    return (<button key={op} onClick={() => createHandleClick(op)}>{op}</button>)
                })}
                <button onClick={() => setValue(evaluate(value))}>{equalSign}</button>
            </div>
        </section>
    )
}